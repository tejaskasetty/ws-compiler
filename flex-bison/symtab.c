struct symtab * symlook(char * s){
	char *p;
	struct symtab *sp;
	for(sp = symtable;sp < &symtable[NUM]; ++sp){
		if(sp->name && !strcmp(sp->name, s))
				return sp;
		if(!sp->name){
			SP->name = strdup(s);
			return sp;
		}	
		
	}
	yyerror("Too many symbols");
	exit(1);
}
