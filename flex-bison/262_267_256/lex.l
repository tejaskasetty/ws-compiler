%{
	/*
		Description: Lexer for the WS compiler.
		             Only valid tokens are whitepace, newline and tabspace.
		             All other characters are considered to be comments.
		Authors:
			Sushrith Arkal (01FB14ECS262)
			Tejas Kasetty (01FB14ECS267)
			Suhas Sumukh (01FB14ECS256)
	*/
	#include <stdio.h>
	#include "y.tab.h"
	#include "symtab.h"
	#include <string.h>
	extern int counter;
	void yyerror(char *);
	int col = 0;
	int line = 0;
%}
%%
" " {	//printf("[Space]");
		++col;
		strcpy(symtable[counter].keyword, "WHITESPACE");
		symtable[counter].value = 0;
		symtable[counter].row = line;
		symtable[counter].col = col;
		++counter;
		return WS;
	}

\t 	{	//printf("[Tab]");
		++col;
		strcpy(symtable[counter].keyword, "TAB");
		symtable[counter].value = 1;
		symtable[counter].row = line;
		symtable[counter].col = col;
		++counter;
		return T;
	}

\n	{	//printf("[LF]\n");
		++line;
		col = 0;
		strcpy(symtable[counter].keyword, "LINEFEED");
		symtable[counter].value = '\n';
		symtable[counter].row = line;
		symtable[counter].col = col; 
		++counter;
		return LF;
	}

.	;

%%
int yywrap(void){
	return 1;
}
