%{
	/*
		Description: Yacc LALR Parser for WS Compiler.
		Authors:
			Sushrith Arkal (01FB14ECS262)
			Tejas Kasetty (01FB14ECS267)
			Suhas Sumukh (01FB14ECS256)
	*/
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <assert.h>
	#include "symtab.h"
	
	#define MAX 1024

	int yylex(void);
	void yyerror(char *);
	
	int counter = 0;
	
	void disp(){
		printf("Symbol Table: \n");
		for(int i = 0; i < counter; ++i)
			printf("\t%s\t%d\t%d\n", symtable[i].keyword, symtable[i].row, symtable[i].col);
	}

	typedef struct label {
		char *s;
	} label_t;

	typedef struct number {
		char *s;
		int val;
	} number_t;

	typedef struct raw {
		char *s;
	} raw_t;

	raw_t * append_raw(raw_t *r, char *c);
	raw_t * create_raw(char *c);
	label_t * create_label(char *c, raw_t * r);
	int label_to_num(label_t *l);
	number_t * create_number(char *s, raw_t *r);
	void strrev(char * s);
%}

%union {
	double dval;
	struct symtab *symtable;
	void * label; //label_t
	void * num;   //number_t
	void * raw;   //raw_t
}

%token WS T LF

%type <label> LABEL
%type <num> NUM
%type <raw> X

%start program

%%

program: 	program statement
		 |
		 ;

statement: 	WS SI 
		 |	T AHI
		 |	LF FCI
		 ;

AHI:		WS AI
		 |	T HI
		 |	LF IOI
		 ;

SI:		 	WS NUM                       {printf("push %d\n", ((number_t *)$2)->val);}
		 |	LF SIB
		 ;

SIB:		WS                           {printf("dup\n");}
		 |	T                            {printf("swap\n");}
		 |	LF                           {printf("pop\n");}
		 ;

AI:			WS ASM
		 |	T DM
		 ;

ASM:		WS                           {printf("add\n");}
		 |	T                            {printf("sub\n");}
		 |	LF                           {printf("mul\n");}
		 ;

DM:			WS                           {printf("div\n");}
		 |	T                            {printf("mod\n");}
		 ;

HI:			WS                           {printf("store\n");}
		 |	T                            {printf("retrieve\n");}
		 ;

FCI:		WS FCA
		 |	T FCB
		 |	LF FCC
		 ;

FCA:		WS LABEL                     {printf("L%d:\n", label_to_num((label_t *)$2));}
		 |	T LABEL                      {printf("call F%d\n", label_to_num((label_t *)$2));}
		 |	LF LABEL                     {printf("goto L%d\n",  label_to_num((label_t *)$2));}
		 ;

FCB:		WS LABEL                     {printf("gotoiz L%d\n", label_to_num((label_t *)$2));}
		 |	T LABEL                      {printf("gotoin L%d\n", label_to_num((label_t *)$2));}
		 |	LF                           {printf("return\n");}
		 ;

FCC:		LF                           {printf("end\n");}
		 ;

IOI:		WS OI
		 |	T RI
		 ;

OI:		 	WS                           {printf("printnum\n");}
		 |	T                            {printf("printchar\n");}
		 ;

RI:			WS                           {printf("readnum\n");}
		 |	T                            {printf("readchar\n");}
		 ;

NUM:		WS X                         {$$ = create_number(" ", (raw_t *)$2);}
		 |	T X                          {$$ = create_number("\t", (raw_t *)$2);}
		 ;

LABEL:		WS X                         {$$ = create_label(" ", (raw_t *)$2);}
		 |	T X                          {$$ = create_label("\t", (raw_t *)$2);}
		 ;

X:			WS X                         {$$ = append_raw((raw_t *)$2, " ");}
		 |	T X                          {$$ = append_raw((raw_t *)$2, "\t");}
		 |	LF                           {$$ = create_raw("\n");}
		 ;

%%

void strrev(char *s) {
	char *f = s;
	char *l = &s[strlen(s)-1];
	while(f < l) {
		char t = *f;
		*f = *l;
		*l = t;
		++f;
		--l;
	}
}

raw_t * append_raw(raw_t *r, char *c) {

	strcat(r->s, c);
	return r;
}

raw_t * create_raw(char *c) {
	raw_t *res = (raw_t *)malloc(sizeof(raw_t));
	res->s = (char *)malloc(sizeof(char) * MAX);
	strcpy(res->s, c);

	return res;
}

label_t * create_label(char *c, raw_t * r) {
	label_t *res = (label_t *) malloc(sizeof(label_t));
	res->s = (char *) malloc(sizeof(char) * MAX);
	strcpy(res->s, c);
	strrev(r->s);
	strcat(res->s, r->s);
	return res;
}

int label_to_num(label_t *l) {
	char *s = l->s;
	int temp = 0;
	for(char *i = s; *s; s++) {
		if(*i == ' ') {
			temp = temp * 2;
		}
		else {
			temp = temp * 2 + 1;
		}
	}
	return temp;
}

number_t * create_number(char *s, raw_t *r) {
	number_t *res = (number_t *) malloc(sizeof(number_t));
	res->s = (char *) malloc(sizeof(char) * MAX);
	strcpy(res->s, s);
	strrev(r->s);
	strcat(res->s, r->s);

	//calculate decimal value of the number
	int temp_num = 0;
	
	for(char *curr = &res->s[1]; *curr != '\n'; curr++) {
		//ws is 0
		//tab is 1
		if(*curr == ' ') {
			temp_num = temp_num * 2;
		}
		else {
			temp_num = temp_num * 2 + 1;
		}
	}

	if(res->s[0] == '\t') {
		temp_num *= -1;
	}

	res->val = temp_num;

	return res;
}

void yyerror(char *s) {
	fprintf(stderr, "Invalid");
	exit(1);
}

int main(void) {
	yyparse();
	//printf("Valid\n");
	return 0;
}

