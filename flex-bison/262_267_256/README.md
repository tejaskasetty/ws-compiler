# Compiler Design Miniproject (Academic Year 2016-2017, CSE Semester 6, PES University)
## Whitespace Esolang Compiler (Intermediate Code Generator)

### Introduction
This is the Compiler Design Miniproject submission for the academic year of 2016 - 2017, CSE Dept., 6th Sem, PES University.  
Whitespace is an esolang in which the only valid tokens are whitespace (`" "`), tabspace (`"\t"`) and newline (`"\n"`).  
This is a stack based language, with a heap that can store data.

### Instructions
Run the following commands in a UNIX environment with `flex` and `bison` and `gcc-4.8.5`:
```
	$ make
	$ ./ws < INPUT_FILE
```

We have included 2 sample input files (`count.ws` and `main.ws`).

## Authors
```
	Sushrith Arkal (01FB14ECS262)
	Tejas Kasetty (01FB14ECS267)
	Suhas Sumukh (01FB14ECS256)
```
