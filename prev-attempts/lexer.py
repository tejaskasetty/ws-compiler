#!/usr/bin/python3

class token:
	WHITESPACE = ' '
	TABSPACE = '\t'
	LINEFEED = '\n'
	def __init__(self, tok_type, line, col):
		self.tok_type = tok_type
		self.line = line
		self.col = col

	def _print_token(self):
		tok_typestr = None
		if(self.tok_type == token.WHITESPACE):
			tok_type_str = 'WHITESPACE'
		if(self.tok_type == token.TABSPACE):
			tok_type_str = 'TABSPACE'
		if(self.tok_type == token.LINEFEED):
			tok_type_str = 'LINEFEED'
		print(tok_type_str, 'line', self.line, 'col', self.col)

class lexer:

	def __init__(self, filename):
		self.filename = filename
		self.file = open(filename, 'r')
		self.curr_line = 1
		self.curr_col = 1
	
	def __iter__(self): 
		return self

	def __next__(self): # removed the get_next_token with __next__
		tok = None
		while tok == None:					 # added a looping sturcture -- 
			curr_lexeme = self.file.read(1)  #       to avoid return of tok(None) for comments
			if curr_lexeme == '':
				raise StopIteration
			if(curr_lexeme == token.WHITESPACE):
				tok = token(token.WHITESPACE, self.curr_line, self.curr_col)
			elif(curr_lexeme == token.TABSPACE):
				tok = token(token.TABSPACE, self.curr_line, self.curr_col)
			elif(curr_lexeme == token.LINEFEED):
				self.curr_line += 1
				self.curr_col = 0
				tok = token(token.LINEFEED, self.curr_line, self.curr_col)
			self.curr_col += 1
		return tok

	def close(self):
		self.file.close()

if __name__ == '__main__':
	lex = lexer('count.ws')
	tokgen = iter(lex)
	print(type(tokgen))
	for tok in tokgen:
		tok._print_token()
	lex.close()