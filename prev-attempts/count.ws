"""
class Test :
	def __init__(self) :
		print("ctor")
	def __iter__(self) :
		print("iter")
		return self
	def __next__(self) :
		print("next")
	
t = Test()
i = iter(t)
next(i)
"""
"""
class Test :
	def __init__(self) :
		print("ctor")
	def __iter__(self) :
		print("iter")
		self.i = 10
		return self
	def __next__(self) :
		print("next")
		self.i += 1
		return self.i
	
t = Test()
i = iter(t)
print(type(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))
"""
"""
class MulTable :
	def __init__(self, n) :
		#print("ctor")
		self.n = n
	def __iter__(self) :
		#print("iter")
		self.i = self.n
		return self
	def __next__(self) :
		self.i += self.n
		return self.i - self.n
m = MulTable(5)
i = iter(m)
for multiple in range(10) :
		print(next(i))
"""
# position of self.i matters.
# if the position of an iterable is initialized in the ctor,
#	we can walk thro the iterable only once
# if the position of an iterable is initialized in the iter function,
#	we can walk thro the iterable any # of times simultanoeusly
# if # if 
class MyStrings :
	def __init__(self, myiterable) :
		self.myiterable = myiterable
	def __iter__(self) :
		#print("iter called")
		self.i = 0
		return self
	def __next__(self) :
		#print("next called")
		while self.i < len(self.myiterable) :
			self.i += 1
			return self.myiterable[self.i - 1]
		else:
			raise StopIteration
a = MyStrings(('ravana', 'duryodhana', 'bali', 'ghatodkaja'))

"""
print("one")
for i in a :
	print(i)
print("two")
for i in a :
	print(i)
print("three")
"""
i1 = iter(a)
i2 = iter(a)
print (next(i1), next(i2))
print(i1 == i2) # True












